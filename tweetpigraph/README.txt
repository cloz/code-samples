=== Tweetpigraph ===
Tags: twitter, epigraph
Requires at least: 4.0
Tested up to: 4.6
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Stable tag: 0.6.0

Adds epigraph from tweet.

== Description ==

Tweetpigraph gets data from given tweet and creates epigraph on the page.

== Installation ==

1. Upload the whole plugin folder to your /wp-content/plugins/ folder.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Get an API key at https://apps.twitter.com/ and enter it in the plugin settings of your website

== Changelog ==

= 0.6.0 =
*Release Date - September the 15th, 2016*

* Added: i18n support
* Fixed: empty Twitter status ID input now unlinks tweet from post
* Fixed: fixed several typos & absolute paths/names to relative


= 0.5.0 =
*Release Date - September the 14th, 2016*

* Initial release