<?php

/*
 * Admin-side plugin functionality
 */

class Tweetpigraph_Admin {
    
    // Variables, unified
    private $plugin_name;
    private $version;

    // Error notificaton
    private $notify;

    // Initialize the class
    public function __construct( $plugin_name, $version ) {
        $this->plugin_name  = $plugin_name;
        $this->version      = $version;
    }

    // Register the settings page
    public function add_settings_page() {
        add_options_page( esc_html__( 'Tweetpigraph Settings', $this->plugin_name ), 'Tweetpigraph', 
            'manage_options', $this->plugin_name, array( $this, 'display_settings_page' ) );
    }
    
    // Display the settings page
    public function display_settings_page() {
        include_once( 'display/display-' . $this->plugin_name . '-settings.php' );
    }
    
    // Add 'Settings' link to the plugins page
    public function add_action_links ( $links ) {
        $mylinks = array(
        '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' .__('Settings', $this->plugin_name) . '</a>',
        );
        return array_merge( $mylinks, $links );
    }

    // Initialize options
    public function settings_init() {
        // ID Fix
        ini_set('precision', 20);

        // Register settings
        register_setting(
            $this->plugin_name . '_options',                                // Option group
            $this->plugin_name . '_option',                                 // Option name
            array($this, $this->plugin_name . '_settings_validate' )        // Validation callback
        );

        // Twitter API Section
        add_settings_section(
            $this->plugin_name . '_api_settings_section',                   // ID of the section
            __( 'Twitter API Options', $this->plugin_name ),                // Title
            array( $this, $this->plugin_name . '_description_callback'),    // Callback function name
            $this->plugin_name                                              // Page to add settings on
        );

        // Twitter API Consumer Key Setting
        add_settings_field( 
            $this->plugin_name . '_consumer_key',                           // ID of the field
            __( 'Consumer Key', $this->plugin_name ),                       // Label
            array( $this, 'input_callback_render' ),                        // Callback function name
            $this->plugin_name,                                             // Page to add settings on
            $this->plugin_name . '_api_settings_section',                   // ID of the section the field belongs to
            array( 'option' => 'consumer_key' )                             // Arguments for callback function
        );

        // Twitter API Consumer Secret Setting
        add_settings_field( 
            $this->plugin_name . '_consumer_secret',
            __( 'Consumer Secret', $this->plugin_name ),
            array( $this, 'input_callback_render' ),
            $this->plugin_name,
            $this->plugin_name . '_api_settings_section',
            array( 'option' => 'consumer_secret' )
        );

        // Twitter API Access Token Setting
        add_settings_field( 
            $this->plugin_name . '_token',
            __( 'Access Token', $this->plugin_name ),
            array( $this, 'input_callback_render' ),
            $this->plugin_name,
            $this->plugin_name . '_api_settings_section',
            array( 'option' => 'token' )
        );

        // Twitter API Access Token Secret Setting
        add_settings_field( 
            $this->plugin_name . '_token_secret',
            __( 'Access Token Secret', $this->plugin_name ),
            array( $this, 'input_callback_render' ),
            $this->plugin_name,
            $this->plugin_name . '_api_settings_section',
            array( 'option' => 'token_secret' )
        );

    }

    // Note for user above the settings
    public function tweetpigraph_description_callback () {
        printf( wp_kses( __('Twitter API keys available <a href="%1$s" target="_blank">here</a>.
                For more information check Instructions tab', $this->plugin_name ),
                array(  'a' => array( 'href' => array(), 'target' => array() ) ) ), 
                esc_url( 'https://apps.twitter.com/' ) );
    }

    // Callback render for inputs
    public function input_callback_render( array $args ) {
        $options = get_option( $this->plugin_name . '_option' );
        $option = $args['option'];
        if ( !isset( $options[$option] ) ) {
            $options[$option] = "" ;
        }

        // HTML Input field for options
        printf(
            '<input type="text" name="%1$s[%2$s]" id="%3$s" value="%4$s" class="large-text">',
            $this->plugin_name . '_option',
            $option,
            $this->plugin_name . "_input_" . $option,
            $options[$option]
        );
    }

    // Validate settings
    public function tweetpigraph_settings_validate( $input ) {
        $options = get_option( $this->plugin_name . '_option' );

        // Basic trim operations
        $options['consumer_key']    = trim( $input['consumer_key'] );
        $options['consumer_secret'] = trim( $input['consumer_secret'] );
        $options['token']           = trim( $input['token'] ); 
        $options['token_secret']    = trim( $input['token_secret'] );

        /* TODO: convenient input check */

        return $options;
    }

    // Create post input box
    public function add_meta_box( $post_type ) {
        add_meta_box(
            $this->plugin_name . '_meta_box',                               // Meta Box ID
            esc_html__( 'Tweetpigraph', $this->plugin_name ),               // Title
            array( $this, 'display_meta_box' ),                             // Callback function
            'post',                                                         // Admin page / post type
            'side',                                                         // Context
            'high'                                                          // Priority
            );
    }

    // Display meta box
    public function display_meta_box( $post ) {
        
        global $value;

        // Get current value from database
        $value = get_post_meta( $post -> ID, '_' . $this->plugin_name, true );
        if ( !isset( $value ) ) {
            $value = "";
        }

        // Show display
        require_once( 'display/display-' . $this->plugin_name . '-meta-box.php' );
    }

    // Save post
    public function save_post( $post_id ) {
        
        // Check save status
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce   = ( isset( $_POST[ $this->plugin_name . '_nonce' ] ) 
            && wp_verify_nonce( $_POST[ $this->plugin_name . '_nonce' ], $this->plugin_name . '_nonce_action' ) ) ? 'true' : 'false';
     
        // Exit script depending on save status
        if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
            return;
        }
     
        // Check for input & then...
        $tweet_id = $this->plugin_name . '_meta_box';
        if ( isset( $_POST[ $tweet_id ] ) 
            && ( $_POST[ $tweet_id ] <> '' ) ) {
            // Work with tweets and save all data; notify if error
            $tweet_worker = new Tweetpigraph_Worker( $this->plugin_name, $this->version );
            $this->notify = $tweet_worker->work( $post_id, sanitize_text_field( $_POST[ $tweet_id ] ) );
        } else {
            // Check if post already has tweetpigraph
            if ( '' <> get_post_meta( $post_id, '_' . $this->plugin_name, true ) ) {
                // Unlink tweet from post
                delete_post_meta( $post_id, '_' . $this->plugin_name );
            }
            
        }
    }

    // Notifier
    public function notify( $messages ) {
        // Select error
        switch ( $this->notify ) {
            case 'twitter error':
                add_settings_error( $this->plugin_name . '-notices',
                        $this->plugin_name . '-twitter',
                        sprintf( wp_kses( __( 'Please input <a href="%1$s">your Twitter API keys</a>.', $this->plugin_name ),
                        array(  'a' => array( 'href' => array() ) ) ), 
                        admin_url( 'options-general.php?page=' . $this->plugin_name ) );
                        'error' );
                break;
            case 'input keys':
                add_settings_error( $this->plugin_name . '-notices',
                        $this->plugin_name . '-keys', 
                        __('Twitter error. Check Status ID and try again', $this->plugin_name ),
                        'error' );
                break;
            }

        // Show error    
        settings_errors( $this->plugin_name . '-notices' );
    }

    // Add settings CSS
    public function enqueue_styles() {
        /* Future use
        if ( 'settings_page_'  . $this->plugin_name == get_current_screen() -> id ) {
            wp_enqueue_style( $this->plugin_name . '_admin_styles', 
                plugin_dir_url( __FILE__ ) . 'display/' . $this->plugin_name . '-admin.css', 
                array(),
                $this->version,
                'all' );
        }
        */
    }

    // Add settings Javascript
    public function enqueue_scripts() {
        if ( 'settings_page_' . $this->plugin_name == get_current_screen() -> id ) {
            wp_enqueue_script( $this->plugin_name . '_admin_scripts', 
                plugin_dir_url( __FILE__ ) . 'display/' . $this->plugin_name . '-admin.js', 
                array( 'jquery' ),
                $this->version,
                false );
        }
    }

}