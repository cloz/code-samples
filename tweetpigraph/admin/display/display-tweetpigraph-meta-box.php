<?php
/**
 * Meta box display
 */

// Add a nonce field to check it later
wp_nonce_field( $this->plugin_name . '_nonce_action', $this->plugin_name . '_nonce' );

?>

<div class="wrap">
    <label for="<?php echo $this->plugin_name . '_meta_box';?>"><?php esc_html_e( 'Twitter Status ID', $this->plugin_name ); ?></label>
    <input name="<?php echo $this->plugin_name . '_meta_box'; ?>" class="large-text" type="text" value="<?php echo $value; ?>">
    <span class="description"><?php esc_html_e( 'Input twitter status ID here',  $this->plugin_name ); ?></span>
</div>