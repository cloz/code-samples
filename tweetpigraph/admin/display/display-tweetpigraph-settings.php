<?php
/**
 * Settings display
 */
?>

<div class="wrap">
    <h2><?php esc_html_e( get_admin_page_title() ); ?></h2>

    <h2 class="nav-tab-wrapper">
        <a href="#twitter-api" class="nav-tab"><?php esc_html_e( 'Twitter API Options', $this->plugin_name ); ?></a>
        <a href="#instructions" class="nav-tab"><?php esc_html_e( 'Instructions', $this->plugin_name ); ?></a>
    </h2>

    <div id="twitter-api" class="wrap metabox-holder">
        <form method="post" action="options.php">
            <?php 
                settings_fields( $this->plugin_name . "_options" );
                do_settings_sections( $this->plugin_name );
                submit_button( __( 'Save Changes', $this->plugin_name ) );
            ?>
        </form> 
    </div>

    <div id="instructions" class="wrap metabox-holder">
        <h2><?php esc_html_e( 'Instructions', $this->plugin_name ); ?></h2>
        <span><?php esc_html_e( 'Tweetpigraph needs Twitter authorization to work. So please follow these steps:', $this->plugin_name ); ?></span>
        <ol>
            <li><?php printf( wp_kses( __( 'Go to the <a href="%s" target="_blank">Twitter Application Management</a> website. 
                        You may need to login using your Twitter user name and password.', $this->plugin_name ), 
                        array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 
                        esc_url( 'https://apps.twitter.com/' ) );
                ?></li>
            <li><?php esc_html_e( 'If you have not created applications yet, click ‘Create New App’.', $this->plugin_name ); ?></li>
            <li><?php esc_html_e( 'Fill the fields:', $this->plugin_name ); ?>
                <ul>
                    <li><?php echo wp_kses( __( '<strong>Name</strong> — the name for your application', $this->plugin_name ), array( 'strong' => array() ) ); ?></li>
                    <li><?php echo wp_kses( __( '<strong>Description</strong> — short description for the application', $this->plugin_name ), array( 'strong' => array() ) ); ?></li>
                    <li><?php echo wp_kses( __( '<strong>Website</strong> — you can use your website url ', $this->plugin_name ), array( 'strong' => array() ) );
                            if ( !empty( get_bloginfo ( 'url' ) ) ) { echo "(<strong>" . get_bloginfo ( 'url' ) . "</strong>)"; } ?></li>
                    <li><?php echo wp_kses( __( '<strong>Callback URL</strong> — leave it blank', $this->plugin_name ), array( 'strong' => array() ) ); ?></li>
                </ul>
            </li>
            <li><?php esc_html_e( 'Read and agree with Developer Agreement then click on ‘Create your Twitter application’', $this->plugin_name ); ?></li>
            <li><?php esc_html_e( 'Select ‘Keys and Access Tokens’ tab and press ‘Create my access token’ button', $this->plugin_name ); ?></li>
            <li><?php echo wp_kses( __( 'Now you can copy <strong>Consumer key</strong>, <strong>Consumer secret</strong>,
                <strong>Access token</strong> and <strong>Access token secret</strong> from your Twitter application page into the plugin settings',
                    $this->plugin_name ), array( 'strong' => array() ) ); ?></li>
            <li><?php esc_html_e( 'Save changes', $this->plugin_name ); ?></li>
        </ol>
    </div>

</div>