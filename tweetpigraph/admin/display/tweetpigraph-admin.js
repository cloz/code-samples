(function( $ ) {

$( document ).ready(function() {
    // Hide instruction on load
    $( "#instructions" ).addClass( "hidden" );
    $( ".nav-tab" ).first().addClass( "nav-tab-active" );

    // Toggle tabs
    $( '.nav-tab-wrapper' ).on( 'click', 'a', function( e ) {
        e.preventDefault();

        // Tabs toggle
        $( '.nav-tab' ).removeClass('nav-tab-active');
        $( this ).addClass( "nav-tab-active" );

        // Content toggle
        $( ".metabox-holder" ).addClass( "hidden" );
		$( $( this ).attr( "href" ) ).toggleClass( "hidden" );
    });
});

})( jQuery );