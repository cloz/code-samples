<?php

/*
 * Runs when user activates plugin
 */

class Tweetpigraph_Activator {

    // Plugin name
    static $plugin_name = 'tweetpigraph';

    // Activate plugin
    public static function activate() {

        global $wpdb;

        /* Version placeholder for future use */

        // Database character set & collation
        $charset_collate = $wpdb->get_charset_collate();

        // Plugin table name
        $table_name = $wpdb->prefix . self::$plugin_name . '_statuses';

        $sql = "CREATE TABLE $table_name (
                tweet_id bigint NOT NULL,
                text nvarchar(140) NULL,
                author nvarchar(20) NULL,
                nickname varchar(15) NULL,
                PRIMARY KEY tweet_id (tweet_id)
                ) $charset_collate;";

        // Create table if have not created already
        if( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name ) {
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }

}

?>