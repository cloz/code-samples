<?php

/*
 * Database queries
 */

class Tweetpigraph_DB {

    static $plugin_name = 'tweetpigraph';

    // Get plugin table name
    private static function get_table_name() {
        global $wpdb;
        $table_name = $wpdb->prefix . static::$plugin_name . '_statuses';
        return $table_name;
    }

    // Create
    static function insert( $data ) {
        global $wpdb;
        $wpdb->insert( self::get_table_name(), $data );
    }

    // Read, limit 1
    static function select( $id ) {
        global $wpdb;
        $sql = sprintf( "SELECT * FROM %s WHERE tweet_id = %%s", self::get_table_name() );
        return $wpdb->get_row( $wpdb->prepare( $sql, $id ) );
    }

    // Read, unlimited
    static function select_all( $id ) {
        global $wpdb;
        $sql = sprintf( "SELECT * FROM %s WHERE tweet_id = %%s", self::get_table_name() );
        return $wpdb->get_results( $wpdb->prepare( $sql, $id ) );
    }

    // Update
    static function update( $data, $where ) {
        global $wpdb;
        $wpdb->update( self::get_table_name(), $data, $where );
    }

    // Delete
    static function delete( $id ) {
        global $wpdb;
        $wpdb->delete( self::get_table_name(), $id);
    }

}