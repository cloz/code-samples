<?php

class Tweetpigraph_Deactivator {

    // Static variable
    static $plugin_name = 'tweetpigraph';

    // Dectivate plugin
    public static function deactivate() {
        unregister_setting( static::$plugin_name . "_options", static::$plugin_name . "_option");
    }

}

?>