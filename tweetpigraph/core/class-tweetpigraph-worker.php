<?php
/*
 * Core functionality
 */

class Tweetpigraph_Worker {
    
    // Variables, unified
    private $plugin_name;
    private $version;

    // Tweet
    private $tweet_json;
    private $tweet_decoded;

    // Initialize the class
    public function __construct( $plugin_name, $version ) {
        $this->plugin_name  = $plugin_name;
        $this->version      = $version;
    }
    
    // Start work
    public function work( $post_id, $tweet_id ) {
        // Get response from twitter
        if ( $this->get_tweet_data( $tweet_id ) ) {
            
            // Validate tweet data
            if ( $this->validate_tweet() ) {
                
                // Save data to database
                $this->save_tweet_data( $tweet_id );
                
                // Save tweet status id
                update_post_meta( $post_id, '_' . $this->plugin_name, $tweet_id );

                return 'success';
            } else {
                // Send error:
                return 'twitter error';
            }
        } else {
            // Send error:
            return 'input keys';
        }
    }

    // Get tweet data
    private function get_tweet_data( $tweet_id ) {
        require_once( plugin_dir_path( dirname( __FILE__ ) ) . '/lib/codebird.php' );

        // Load options
        $options = get_option( $this->plugin_name . '_option' );
        if ( !empty( $options['consumer_key'] )
             && !empty( $options['consumer_secret'] )
             && !empty( $options['token'] )
             && !empty( $options['token_secret'] ) 
        ) {
            // Use common library for twitter
            \Codebird\Codebird::setConsumerKey( $options['consumer_key'], $options['consumer_secret'] );
            $cb = \Codebird\Codebird::getInstance();
            $cb->setToken( $options['token'], $options['token_secret'] );
            $cb->setReturnFormat(CODEBIRD_RETURNFORMAT_JSON);

            // Save response
            $this->tweet_json =  $cb->statuses_show_ID("id=$tweet_id");

            return true;
        } else {
            // Return error: enter OAuth keys
            return false;
        }
    }

    // Validate tweet
    private function validate_tweet() {
        // Decode JSON object to array
        $decoded = json_decode( $this->tweet_json );
        
        // Check if error 
        if ( isset( $decoded->error ) ) {
            return false;
            // TODO: check for different errors
        }

        // Check if data is here
        if ( ( empty( $decoded->id ) )
            || ( empty( $decoded->text ) ) 
            || ( empty( $decoded->user->name ) )
            || ( empty( $decoded->user->screen_name ) )
        ) {
            return false;
        }

        // Save decoded tweet
        $this->tweet_decoded = $decoded;

        return true;
    }

    // Save tweet data to database
    private function save_tweet_data ( $tweet_id ) {
        require_once( plugin_dir_path( dirname( __FILE__ ) ) . '/core/class-' . $this->plugin_name . '-db.php' );

        // Check if already have tweet id in base; object to array convertion
        $already_have = json_decode( json_encode( Tweetpigraph_DB::select( $tweet_id ) ), true);

        if ( count( $already_have ) == 0 ) {
            // Save tweet data
            Tweetpigraph_DB::insert( array(
                        'tweet_id' => $this->tweet_decoded->id,
                        'text' => $this->tweet_decoded->text,
                        'author' => $this->tweet_decoded->user->name,
                        'nickname' => $this->tweet_decoded->user->screen_name, ) );
        }
    }
}

?>