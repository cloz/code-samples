<?php

/*
 * Main class
 */

class Tweetpigraph {

    // Variables, unified
    private $plugin_name;
    private $version;

    // Path variable
    private $plugin_path;

    // Initialize the class
    public function __construct() {
        $this->plugin_name      = 'tweetpigraph';
        $this->version          = '0.6.0';
        $this->plugin_path      = plugin_dir_path( dirname( __FILE__ ) );

        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    // Standard getters
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_version() {
        return $this->version;
    }

    // Load the dependencies
    private function load_dependencies() {
        // Core part
        require_once $this->plugin_path . 'core/class-' . $this->plugin_name . '-worker.php';

        // Admin part
        require_once $this->plugin_path . 'admin/class-' . $this->plugin_name . '-admin.php';

        // Public part
        require_once $this->plugin_path . 'public/class-' . $this->plugin_name . '-public.php';
    }

    // Register all admin hooks
    private function define_admin_hooks() {
        $plugin_admin = new Tweetpigraph_Admin( $this->get_plugin_name(), $this->get_version() );

        /* Load admin CSS placeholder */

        // Load admin JS
        add_action( 'admin_enqueue_scripts', array( $plugin_admin, 'enqueue_scripts' ) );

        // Add menu item
        add_action( 'admin_menu', array( $plugin_admin, 'add_settings_page' ) );

        // Create settings part
        add_action('admin_init', array( $plugin_admin, 'settings_init' ) );

        // Create post input block to post type
        add_action('add_meta_boxes_post', array( $plugin_admin, 'add_meta_box' ) );

        // Save the data on post save
        add_action('save_post', array( $plugin_admin, 'save_post' ) );

        // Notify
        add_action( 'admin_notices', array( $plugin_admin, 'notify' ) );

        // Add 'Settings' link to the plugins page
        add_filter( 'plugin_action_links_' . plugin_basename( $this->plugin_path . $this->plugin_name . '.php' ), 
            array( $plugin_admin, 'add_action_links' ) );
    }

    // Register all public hooks 
    private function define_public_hooks() {
        $plugin_public = new Tweetpigraph_Public( $this->get_plugin_name(), $this->get_version() );

        // Register public CSS
        add_action( 'wp_enqueue_scripts', array( $plugin_public, 'register_styles' ) );

        /* Register public JS placeholder */

        // Load data on post load
        add_action('the_content', array( $plugin_public, 'show_epigraph' ) );
    }

    // Set locale
    public function set_locale() {
        load_plugin_textdomain( $this->plugin_name, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

}
