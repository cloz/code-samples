<?php

/*
 * Public-side plugin functionality
 */

class Tweetpigraph_Public {

    // Variables, unified
    private $plugin_name;
    private $version;

    // Initialize the class
    public function __construct( $plugin_name, $version ) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    // Show epigraph on post
    public function show_epigraph( $content ) {

    	global $tweet_id;

    	// Get twitter status ID from the database
    	$tweet_id = get_post_meta( get_the_ID(), '_' . $this->plugin_name, true );
 
    	// Checks and displays the data
    	if( !empty( $tweet_id ) ) {

            require_once( plugin_dir_path( dirname( __FILE__ ) ) . '/core/class-' . $this->plugin_name . '-db.php' );

            // Check current post ID and pass it to meta
            global $post;
            $tweet_id = get_post_meta( $post->ID, '_' . $this->plugin_name, true);

            // Get the actual data from database; object to array convertion
            $values = json_decode( json_encode( Tweetpigraph_DB::select( $tweet_id ) ), true);

    		// Use buffer & output capture
    		ob_start();                      
			include_once('display/display-' . $this->plugin_name . '-epigraph.php');
			$epigraph = ob_get_contents();    
			ob_end_clean();

			// Merge                  
    		$content = $epigraph . $content;

            // Add public CSS
            wp_enqueue_style( $this->plugin_name . '_public_styles' );
    	}

        // Show post content
		return $content; 

    }

    // Register public CSS
    public function register_styles() {
        wp_register_style( $this->plugin_name . '_public_styles',
            plugin_dir_url( __FILE__ ) . 'display/' . $this->plugin_name . '-public.css',  
            array(),
            $this->version,
            'all' ); 
    }

    // Register public Javascript
    public function register_scripts() {
        /* For future use
        wp_register_script( $this->plugin_name . '_public_scripts', 
            plugin_dir_url( __FILE__ ) . 'display/' . $this->plugin_name . '-public.js', 
            array( 'jquery' ),
            $this->version,
            false );
        */
    }

}
