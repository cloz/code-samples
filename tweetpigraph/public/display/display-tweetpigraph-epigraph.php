<?php
/**
 * Show epigraph
 */

?>

<div id="twgr-epigraph">
	<div id="twgr-phrase"><?php echo $values['text']; ?></div>
	<div id="twgr-author"><a href="<?php echo "https://twitter.com/" . $values['nickname'] . "/status/" . $values['tweet_id'];
	 ?>" target="_blank"><?php echo $values['author']; ?></a></div>
</div>