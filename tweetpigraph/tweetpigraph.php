<?php

/**
 *
 * @wordpress-plugin
 * Plugin Name:       Tweetpigraph
 * Plugin URI:        
 * Description:       Tweetpigraph gets data from given tweet and creates epigraph on the page.
 * Version:           0.6.0
 * Author:            Alex
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tweetpigraph
 * Domain Path:       /languages/
 *
 */

// Abort if this file is called directly
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Activate plugin
function activate_tweetpigraph() {
	require_once plugin_dir_path( __FILE__ ) . 'core/class-tweetpigraph-activator.php';
	Tweetpigraph_Activator::activate();
}

register_activation_hook( __FILE__, 'activate_tweetpigraph' );

// Deactivate plugin
function deactivate_tweetpigraph() {
	require_once plugin_dir_path( __FILE__ ) . 'core/class-tweetpigraph-deactivator.php';
	Tweetpigraph_Deactivator::deactivate();
}

register_deactivation_hook( __FILE__, 'deactivate_tweetpigraph' );

// Core, loads dependencies and registers hooks
require plugin_dir_path( __FILE__ ) . 'core/class-tweetpigraph.php';

// Start
$tweetpigraph = new Tweetpigraph();

// Set locale
add_action( 'plugins_loaded', array( $tweetpigraph, 'set_locale' ) );

?>