<?php

    // if uninstall.php is not called by WordPress, die
    if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
        exit;
    }

    // Check user's rights
    if ( ! current_user_can( 'activate_plugins' ) ) {
        return;
    }

    // Delete options
    delete_option( "tweetpigraph_option" );
    
    // Delete meta
    delete_post_meta_by_key( '_tweetpigraph' );

    // Drop the table
    global $wpdb;
    $table_name = $wpdb->prefix . 'tweetpigraph_statuses';
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query( $sql );

?>